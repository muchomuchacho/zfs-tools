import sys
import json
import logging
import requests
import pushover_conf

logger = logging.getLogger(__name__)

def pushover(subject, body):
    '''
    Basic function to send notifications using the Pushover
    service API.
    '''
    logger.info('Sending out Pushover notification...')

    push_url = 'https://api.pushover.net/1/messages.json'
    data = {
            'token': pushover_conf.APP_TOKEN,
            'user': pushover_conf.USER_KEY,
            'title': subject,
            'message': body
            }
    headers = {"Content-type": "application/x-www-form-urlencoded"}

    try:
        requests.post(push_url, data=data, headers=headers).raise_for_status()
    except requests.exceptions.HTTPError:
        logger.debug('HTTP connection issues...')
    except requests.exceptions.RequestException as err:
        logger.debug('Unrecoverable error: {}'.format(err))
        sys.exit(1)
