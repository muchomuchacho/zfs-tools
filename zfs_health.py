import time
import socket
import logging
import subprocess
from pushover import pushover as push

__title__ = "zfs_health"
__author__ = "muchomuchacho"
__copyright__ = "Copyright 2017, muchomuchacho"
__license__ = "MIT"
__version__ = "0.1"

problems = False


FORMAT = '%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(filename='/var/log/zfs_health.log', level=logging.DEBUG, format=FORMAT)


class ZFSHealth(object):
    '''
    Set of methods to identify ZFS problems
    '''

    def __init__(self, output=None, scrub_expire=691200):
        self.hostname = socket.gethostname()
        self.notify_subject = "ZFS Health Checks ({!r})".format(
            self.hostname)
        self.scrub_expire = scrub_expire
        self.current_date = time.time()
        self.zpool_names = [x for x in subprocess.check_output(
                ['zpool', 'list', '-H', '-o', 'name']).split() if not 'rpool' in x]
        self.scrub_date = self.get_scrub_date()
        self.dev_logger = logging.getLogger(__name__)
        self.logger = self.dev_logger.getChild('ZFSHealth')
        self.logger.info('Getting started...')

    def check_issues(self):
        '''Check for status keywords to quickly find out if we are dealing
        with a broken array.
        '''

        global problems

        issues = ['DEGRADED', 'FAULTED', 'OFFLINE', 'UNAVAIL',
                  'REMOVED', 'FAIL', 'DESTROYED', 'corrupt',
                  'cannot', 'unrecover']

        logtext = ('Found an issue on {!r}: the zpool is {!r}')
        self.logger.info('Looking for zpool status warnings...')
        for vol in self.zpool_names:
            status = self.get_zpool_status(vol)
            for issue in issues:
                if issue in status:
                    self.logger.info(logtext.format(vol, issue))
                    push(self.notify_subject, logtext.format(vol, issue))
                    problems = True
                    break
                else:
                    self.logger.info('Looking for {!r} warnings. Not found'.format(issue))

    def get_pool_usage(self):
        '''Check what percentage of the pool is being used. For best performance
        a zpool should have between 5% and 20% of space available.
        '''
        global problems

        max_usage = 80
        cap_output = subprocess.check_output(["zpool", "list", "-H",
                                       "-o", "name,capacity"]).split()
        usage = dict(zip(cap_output[0::2], cap_output[1::2]))

        self.logger.info('Checking array capacity status...')
        logtext = ('The zpool {!r} has reached the capacity health check limit. ' +
                   'The capacity status is {!r}')
        for zpool in usage:
            if not 'rpool' in zpool:
                if int(usage[zpool].rstrip("%")) > max_usage:
                    self.logger.info(logtext.format(zpool, usage[zpool]))
                    notify_health = logtext.format(zpool, usage[zpool])
                    push(self.notify_subject, notify_health)
                    problems = True
                else:
                    self.logger.info('Capacity levels are below {!r}%'.format(max_usage))

    def get_disk_errors(self):
        '''Check for any READ, WRITE and/or CKSUM errors
        '''

        global problems

        self.logger.info('Checking for READ/WRITE/CKSUM errors...')
        for vol in self.zpool_names:
            status = self.get_zpool_status(vol)
            for line in status.splitlines():
                if 'ONLINE' in line and not line.startswith(' state'):
                    line = line.replace(" ", "")
                    if line[-3:] != '000':
                        self.logger.info('Drive errors found!')
                        notify_health = 'Drive errors found!'
                        push(self.notify_subject, notify_health)
                        problems = True
                        break
            else:
                self.logger.info('No Drive errors found!')

    def expired_scrub(self):
        '''Check the age of the last scrub. If it's older than 8 days send a warning
        message. The expiration variable is set in seconds; 691200 == 8 days.
        '''

        global problems

        repaired = 'scrub repaired'
        no_request = 'none requested'
        in_progress = 'scrub in progress|resilver'

        logtext = ('ERROR: "zpool scrub {}" needs to be ' +
                   'run before date can be checked')
        self.logger.info('Checking for last scrub date...')
        for vol in self.zpool_names:
            status = self.get_zpool_status(vol)
            if no_request in status:
                self.logger.info(logtext.format(vol))
                break
            elif in_progress in status:
                self.logger.info('Scrub or resilvering in progress. Abort.')
                break
            elif self.current_date - self.scrub_date >= self.scrub_expire:
                if problems is False:
                    notify_subjt = ('Scrub Time Expired.')
                message = ('Pool: {!r} on {!r} needs scrub')
                self.logger.info(message.format(vol, self.hostname))
                notify_message = message.format(vol, self.hostname)
                push(notify_subjt, notify_message)
                problems = True
            else:
                self.logger.info('Scrub for zpool {!r} on {!r} is up to date'.format(vol, self.hostname))

    def get_zpool_status(self, zpool_name):
        return  subprocess.check_output(["zpool", "status", zpool_name])

    def success(self):
        if problems is False:
            push(self.notify_subject, 'Heath checks ran successfully. No errors found!')

    def get_scrub_date(self):
        '''When we run 'zpool status' the output provided tells us the date the
        last run finished. Here we convert that value to the epoch in seconds
        so that we have a reference to calculate the scrub expire state.
        '''

        global problems

        for vol in self.zpool_names:
            status = self.get_zpool_status(vol)
            for line in status.splitlines():
                if line.startswith("  scan"):
                    wordList = line.split()
                    year = wordList[-1]
                    month = wordList[-4]
                    day = wordList[-3]
                    date = str(year) + str(month) + str(day)

        pattern = '%Y%b%d'
        epoch = int(time.mktime(time.strptime(date, pattern)))
        return epoch

    def health_checks(self):
        global problems

        funcs = [self.check_issues, self.get_pool_usage,
                 self.get_disk_errors, self.expired_scrub]
        for fun in funcs:
            if problems is False:
                fun()

        self.success()


run = ZFSHealth()
run.health_checks()
